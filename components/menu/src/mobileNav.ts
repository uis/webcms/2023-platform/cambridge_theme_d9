export default (context: HTMLElement) => {
  const mediaQuery = window.matchMedia("(max-width: 520px)");
  const menuItems = context.querySelector("#menu-items");

  if (!menuItems) {
    throw new Error("No menu items found");
  }

  const mobileMenuButton = context.querySelector<HTMLButtonElement>(
    "#mobile-menu-button",
  );

  if (!mobileMenuButton) {
    throw new Error("No mobile menu button found");
  }

  const mobileMenuWrapper = context.querySelector("#mobile-menu-wrapper");

  if (!mobileMenuWrapper) {
    throw new Error("No mobile menu wrapper found");
  }

  const mobileMenuPanelWrapper = context.querySelector(
    "#mobile-menu-panel-wrapper",
  );

  if (!mobileMenuPanelWrapper) {
    throw new Error("No mobile menu panel wrapper found");
  }

  // Esc key closes mobile menu
  window.addEventListener("keydown", (event) => {
    if (event.key === "Escape") {
      closeMobileMenu();
    }

    // TODO: Handle tab when at the close button
  });

  // Overlay click closes mobile menu
  const overlay = context.querySelector("#menu-overlay");
  if (!overlay) {
    throw new Error("Menu overlay not found");
  }

  overlay.addEventListener("click", () => {
    closeMobileMenu();
  });

  const openMobileMenu = () => {
    mobileMenuPanelWrapper.setAttribute("aria-hidden", "false");
    mobileMenuPanelWrapper.classList.remove("-translate-x-[100vw]");
    mobileMenuPanelWrapper.classList.add("translate-x-0");
  };

  const closeMobileMenu = (focusButton: boolean = true) => {
    mobileMenuPanelWrapper.setAttribute("aria-hidden", "true");
    mobileMenuPanelWrapper.classList.add("-translate-x-[100vw]");
    mobileMenuPanelWrapper.classList.remove("translate-x-0");
    if (focusButton) mobileMenuButton.focus();
  };

  // The hiding and showing of the overlay and the panel contents is handled by the overlay panel logic
  // We just need to ensure the wrapper has slid in and is visible accessibility-wise
  mobileMenuWrapper
    .querySelectorAll("[data-menu-control]")
    .forEach((button) => {
      button.addEventListener("click", () => {
        openMobileMenu();
      });
    });

  const closeButtons = context.querySelectorAll("button[data-close-panel]");
  closeButtons.forEach((button) => {
    button.addEventListener("click", () => {
      closeMobileMenu();
    });
  });

  mediaQuery.addEventListener("change", () => {
    closeMobileMenu(false);

    if (mediaQuery.matches) {
      menuItems.setAttribute("aria-hidden", "true");
      mobileMenuWrapper.setAttribute("aria-hidden", "false");
    } else {
      menuItems.setAttribute("aria-hidden", "false");
      mobileMenuWrapper.setAttribute("aria-hidden", "true");
    }
  });

  mediaQuery.dispatchEvent(new MediaQueryListEvent("change"));
};
